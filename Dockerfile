FROM openjdk:8-jre-alpine
LABEL maintainer="timnjonjo@gmail.com"
RUN apk add --no-cache bash
ENV TZ=Africa/Nairobi
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
EXPOSE 8002
ADD target/*.jar  demo-comic-shujaaz.jar
ENTRYPOINT ["java","-Xmx256m","-jar","/demo-comic-shujaaz.jar"]