package com.timnjonjo.democomic.entities;

import com.timnjonjo.democomic.data.CharacterData;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@Entity
@Table
@NoArgsConstructor
public class Character extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String characterName;
    private String firstName;
    private String lastName;

    @ManyToOne
    @JoinColumn
    private Comic comic;


    public Character(Long id, String characterName, String firstName, String lastName, Comic comic) {
        this.id = id;
        this.characterName = characterName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.comic = comic;
    }

    public Character(Long id, String characterName, String firstName, String lastName) {
        this.id = id;
        this.characterName = characterName;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public CharacterData transform(){
        return new CharacterData(this.id, this.characterName, this.firstName,this.lastName, this.comic.getId());
    }

}
