package com.timnjonjo.democomic.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */

@Data
@Entity
@Table
@NoArgsConstructor
public class Story extends  Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String shortDescription;
    private String text;
    private Date createdAt;
    private Date updatedAt;
    @ManyToOne()
    @JoinColumn
    @JsonManagedReference
    private Comic comic;

    public Story(Long id) {
        this.id = id;
    }


    public Story(Long id, String title, String shortDescription, String text, Comic comic) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.text = text;
        this.comic = comic;
    }

}
