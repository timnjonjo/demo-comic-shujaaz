package com.timnjonjo.democomic.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.timnjonjo.democomic.data.CreatorData;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@Entity
@Table
@NoArgsConstructor
public class Creator extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String salutation;
    private String firstName;
    private String lastName;
    @Column(columnDefinition = "boolean default true")
    private boolean active;
    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY)
    private Collection<Comic>  comics;

    public Creator(Long id, String salutation, String firstName, String lastName, boolean active) {
        this.id = id;
        this.salutation = salutation;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
    }

    public String getFullName(){
        return (this.firstName +" "+ this.lastName).trim();
    }

    public CreatorData transform(){
        return new CreatorData(id, salutation, firstName, lastName, active,getCreatedAt(), getUpdatedAt());
    }
}
