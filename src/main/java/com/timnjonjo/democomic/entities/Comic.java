package com.timnjonjo.democomic.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.timnjonjo.democomic.data.ComicData;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@Entity
@Table
@NoArgsConstructor
public class Comic extends  Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private  boolean active;

    @JoinColumn
    @ManyToOne
    @JsonManagedReference
    private Creator creator;
    @JsonBackReference
    @OneToMany(mappedBy = "comic", cascade = CascadeType.ALL)
    private Collection<Story>  stories;
    @JsonBackReference
    @OneToMany(mappedBy ="comic" ,cascade = CascadeType.ALL, fetch =FetchType.EAGER)
    private Collection<Character> characters;

    public Comic(Long id) {
        this.id = id;
    }

    public Comic(Long id, String title, String description, boolean active, Creator creator) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.active = active;
        this.creator = creator;
    }



    public ComicData transform(){
        return new ComicData(this.id,this.title, this.description,this.active,this.creator.transform(), this.characters.stream().map(Character::transform).collect(Collectors.toList()), this.getCreatedAt(), this.getUpdatedAt());
    }
}

