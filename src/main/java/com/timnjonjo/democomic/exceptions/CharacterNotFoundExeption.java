package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
public class CharacterNotFoundExeption extends Exception {

    public CharacterNotFoundExeption(String message) {
        super(message);
    }

    public CharacterNotFoundExeption(String message, Throwable cause) {
        super(message, cause);
    }
}
