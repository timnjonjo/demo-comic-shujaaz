package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public class CreatorNotFundException extends Exception{

    public CreatorNotFundException(String message) {
        super(message);
    }

    public CreatorNotFundException(String message, Exception cause) {
        super(message, cause);
    }
}
