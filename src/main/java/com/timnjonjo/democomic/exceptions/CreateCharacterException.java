package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
public class CreateCharacterException extends Exception {

    public CreateCharacterException(String message) {
        super(message);
    }

    public CreateCharacterException(String message, Throwable cause) {
        super(message, cause);
    }
}
