package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public class ComicNotFoundException extends Exception {
    public ComicNotFoundException(String message) {
        super(message);
    }

    public ComicNotFoundException(String message, Exception cause) {
        super(message, cause);
    }
}
