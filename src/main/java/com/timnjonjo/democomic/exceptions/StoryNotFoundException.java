package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
public class StoryNotFoundException  extends Exception{

    public StoryNotFoundException(String message) {
        super(message);
    }

    public StoryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
