package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public class CreatorCreationException extends  Exception{

    public CreatorCreationException(String message) {
        super(message);
    }

    public CreatorCreationException(String message, Exception cause) {
        super(message, cause);
    }
}
