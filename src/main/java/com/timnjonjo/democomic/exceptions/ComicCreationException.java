package com.timnjonjo.democomic.exceptions;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public class ComicCreationException extends  Exception {

    public ComicCreationException(String message) {
        super(message);
    }

    public ComicCreationException(String message, Exception cause) {
        super(message, cause);
    }
}
