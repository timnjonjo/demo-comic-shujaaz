package com.timnjonjo.democomic.repositories;

import com.timnjonjo.democomic.entities.Creator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Repository
public interface CreatorRepository  extends JpaRepository<Creator, Long> {
}
