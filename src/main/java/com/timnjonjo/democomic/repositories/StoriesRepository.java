package com.timnjonjo.democomic.repositories;

import com.timnjonjo.democomic.entities.Creator;
import com.timnjonjo.democomic.entities.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Repository
public interface StoriesRepository extends JpaRepository<Story, Long> {

    @Query("SELECT s FROM Story s WHERE s.comic.id=:comicId")
    Collection<Story> findByComicId(Long comicId);
}
