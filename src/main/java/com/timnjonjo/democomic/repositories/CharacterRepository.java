package com.timnjonjo.democomic.repositories;

import com.timnjonjo.democomic.entities.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

    @Query("SELECT c FROM Character c WHERE c.comic.id=:comicId")
    Collection<Character> findByComicId(Long comicId);
}
