/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.timnjonjo.democomic.annotations;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tmwaura
 */
public class EnumValidator implements ConstraintValidator<ValidEnum, String> {

    List<String> statusList = null;

    @Override
    public void initialize(ValidEnum constraint) {
        statusList = new ArrayList<>();
        Class<? extends Enum> enumClass = constraint.enumToValidate();
        Enum[] enumValArr = enumClass.getEnumConstants();
        for (Enum e : enumValArr) {
            statusList.add(e.toString());
        }
    }


    @Override
    public boolean isValid(String obj, ConstraintValidatorContext context) {
        return statusList.contains(obj);
    }
}
