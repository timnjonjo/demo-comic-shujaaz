/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.timnjonjo.democomic.annotations;




import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Tmwaura
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = EnumValidator.class)
public @interface ValidEnum {

    String message() default "Value not allowed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    public Class<? extends Enum> enumToValidate();

}
