package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.entities.Comic;
import com.timnjonjo.democomic.exceptions.ComicCreationException;
import com.timnjonjo.democomic.exceptions.ComicNotFoundException;

import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public interface ComicsService {

    Collection<Comic> findAll();

    Comic create(Comic comicData) throws ComicCreationException;

    Comic getById(Integer comicId) throws ComicNotFoundException;
}
