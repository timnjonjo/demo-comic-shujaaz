package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.StoryData;
import com.timnjonjo.democomic.entities.Story;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.StoryNotFoundException;
import com.timnjonjo.democomic.repositories.StoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Service
public class StoriesServiceImpl implements StoriesService {

    @Autowired
    StoriesRepository   storiesRepository;

    @Override
    public Collection<Story> findAll() {
        return storiesRepository.findAll();
    }

    @Override
    public Story create(StoryData storyData) throws CreateCharacterException {
        try{
            Story story = storyData.transform();
            return storiesRepository.save(story);
        }catch (Exception e){
            throw new CreateCharacterException(e.getMessage(), e);
        }
    }

    @Override
    public Story getById(Integer storyId) throws StoryNotFoundException {
        Optional<Story> story = storiesRepository.findById(Long.valueOf(storyId));
        if(story.isPresent()){
            return story.get();
        }else {
            throw new StoryNotFoundException(String.format("Story with Id : %s Not Found", storyId));
        }
    }

    @Override
    public Collection<Story> getByComicId(Integer comicId) throws StoryNotFoundException {
        try{
            return storiesRepository.findByComicId(Long.valueOf(comicId));
        } catch (Exception e){
            throw new StoryNotFoundException(String.format("Stories for Comic Id : %s Not Found", comicId));
        }
    }
}
