package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.CharacterData;
import com.timnjonjo.democomic.entities.Character;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.CharacterNotFoundExeption;

import java.util.Collection;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
public interface CharactersService {
    Collection<Character> findAll();

    Character create(CharacterData characterData) throws CreateCharacterException;

    Character getById(Integer storyId) throws CharacterNotFoundExeption;

    Collection<Character> getByComicId(Integer comicId) throws CharacterNotFoundExeption;;
}
