package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.CreatorData;
import com.timnjonjo.democomic.entities.Creator;
import com.timnjonjo.democomic.exceptions.CreatorCreationException;
import com.timnjonjo.democomic.exceptions.CreatorNotFundException;
import com.timnjonjo.democomic.repositories.CreatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Collection;
import java.util.Optional;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Service
public class CreatorsServiceImpl implements CreatorsService {

    @Autowired
    CreatorRepository creatorRepository;

    @Override
    public Collection<Creator> findAll() {
        return creatorRepository.findAll();
    }

    @Override
    public Creator create(CreatorData creatorData) throws CreatorCreationException {
        Creator creator = creatorData.transform();
        try {
            return creatorRepository.save(creator);
        }catch (Exception e){
            throw new CreatorCreationException(e.getMessage(), e);
        }
    }

    @Override
    public Creator getById(Integer creatorId) throws CreatorNotFundException {
        Optional<Creator> creator = creatorRepository.findById(Long.valueOf(creatorId));
        if(creator.isPresent()){
            return creator.get();
        }else {
            throw new CreatorNotFundException(String.format("Creator with Id : %s Not Found", creatorId));
        }
    }
}
