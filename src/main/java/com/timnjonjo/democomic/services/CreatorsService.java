package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.CreatorData;
import com.timnjonjo.democomic.entities.Creator;
import com.timnjonjo.democomic.exceptions.CreatorCreationException;
import com.timnjonjo.democomic.exceptions.CreatorNotFundException;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public interface CreatorsService {
    Collection<Creator> findAll();
    Creator create(CreatorData creatorData) throws CreatorCreationException;

    Creator getById(Integer creatorId) throws CreatorNotFundException;
}
