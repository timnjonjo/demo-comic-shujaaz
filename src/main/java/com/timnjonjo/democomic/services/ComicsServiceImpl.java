package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.entities.Comic;
import com.timnjonjo.democomic.exceptions.ComicCreationException;
import com.timnjonjo.democomic.exceptions.ComicNotFoundException;
import com.timnjonjo.democomic.repositories.ComicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Service
public class ComicsServiceImpl implements ComicsService {
    @Autowired
    ComicsRepository comicsRepository;

    @Override
    public Collection<Comic> findAll() {
        return comicsRepository.findAll();
    }

    @Override
    public Comic create(Comic comicData) throws ComicCreationException {
        try {
            return comicsRepository.save(comicData);
        }catch (Exception e){
            throw new ComicCreationException(e.getMessage(), e);
        }
    }

    @Override
    public Comic getById(Integer comicId) throws ComicNotFoundException {
        Optional<Comic> creator = comicsRepository.findById(Long.valueOf(comicId));
        if(creator.isPresent()){
            return creator.get();
        }else {
            throw new ComicNotFoundException(String.format("Comic  with Id : %s Not Found", comicId));
        }
    }


}
