package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.StoryData;
import com.timnjonjo.democomic.entities.Story;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.StoryNotFoundException;

import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public interface StoriesService {
    Collection<Story> findAll();

    Story create(StoryData storyData) throws CreateCharacterException;

    Story getById(Integer storyId) throws StoryNotFoundException;

    Collection<Story> getByComicId(Integer comicId) throws StoryNotFoundException;
}
