package com.timnjonjo.democomic.services;

import com.timnjonjo.democomic.data.CharacterData;
import com.timnjonjo.democomic.entities.Character;
import com.timnjonjo.democomic.entities.Story;
import com.timnjonjo.democomic.exceptions.CharacterNotFoundExeption;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.StoryNotFoundException;
import com.timnjonjo.democomic.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * @author TMwaura on 06/10/2020
 * @Project demo-comic
 */
@Service
public class CharactersServiceImpl implements CharactersService {
    @Autowired
    CharacterRepository characterRepository;

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character create(CharacterData characterData) throws CreateCharacterException {
        try {
            Character character = characterData.transform();
            return  characterRepository.save(character);
        }catch (Exception exception){
            throw new CreateCharacterException(exception.getMessage(), exception);
        }
    }

    @Override
    public Character getById(Integer storyId) throws CharacterNotFoundExeption {
        Optional<Character> character = characterRepository.findById(Long.valueOf(storyId));
        if(character.isPresent()){
            return character.get();
        }else {
            throw new CharacterNotFoundExeption(String.format("Character with Id : %s Not Found", storyId));
        }
    }

    @Override
    public Collection<Character> getByComicId(Integer comicId) throws CharacterNotFoundExeption {
        try {
            return  characterRepository.findByComicId(Long.valueOf(comicId));
        }catch (Exception exception){
            throw new CharacterNotFoundExeption(exception.getMessage(), exception);
        }
    }
}
