package com.timnjonjo.democomic.data;

import com.timnjonjo.democomic.entities.Character;
import com.timnjonjo.democomic.entities.Comic;
import lombok.Data;
import lombok.ToString;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@ToString
public class ComicData {
    private Long id;
    private String title;
    private String description;
    private  boolean active;
    private CreatorData creator;
    private Collection<CharacterData> characters;
    private Date createdAt;
    private Date updatedAt;


    public ComicData(Long id, String title, String description, boolean active,CreatorData creator, Collection<CharacterData> characters, Date createdAt, Date updatedAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.active = active;
        this.creator = creator;
        this.characters = characters;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

//    public Comic transform(){
//        return new Comic(this.id, this.title, this.description, this.active , creator.transform(), this.characters.stream().map(CharacterData::transform).collect(Collectors.toList()));
//    }

    public Comic transform(){
        return new Comic(this.id, this.title, this.description, this.active , creator.transform());
    }


}
