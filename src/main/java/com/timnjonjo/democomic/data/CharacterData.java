package com.timnjonjo.democomic.data;

import com.timnjonjo.democomic.entities.Auditable;
import com.timnjonjo.democomic.entities.Character;
import com.timnjonjo.democomic.entities.Comic;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@NoArgsConstructor
public class CharacterData extends Auditable {

    private Long id;
    private String characterName;
    private String firstName;
    private String lastName;
    private Long comicId;


    public CharacterData(Long id, String characterName, String firstName, String lastName, Long comicId) {
        this.id = id;
        this.characterName = characterName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.comicId = comicId;
    }


    public Character transform(){
        return new Character(this.id, this.characterName,this.firstName, this.lastName, new Comic(comicId));
    }


}
