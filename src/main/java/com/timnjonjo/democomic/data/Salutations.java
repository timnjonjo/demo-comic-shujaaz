package com.timnjonjo.democomic.data;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
public enum Salutations {
    Mr,
    Mrs,
    Miss,
    Dr,
    Ms,
    Prof,
    Rev,
    Lady,
    Sir,
    Capt,
    Major,
}
