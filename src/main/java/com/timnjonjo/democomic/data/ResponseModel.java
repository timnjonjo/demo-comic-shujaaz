package com.timnjonjo.democomic.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseModel {
    private String status;
    private Object data;
    private String message;

    @JsonIgnore
    private HttpStatus httpStatus;

    public ResponseModel(boolean status, Object data, String message, @NotNull  HttpStatus httpStatus){
        this.status = status? "success" :"error";
        this.data=data;
        this.message= message;
        this.httpStatus = httpStatus;
    }

    public ResponseEntity response(){
        return new ResponseEntity(this, this.httpStatus);
    }

}
