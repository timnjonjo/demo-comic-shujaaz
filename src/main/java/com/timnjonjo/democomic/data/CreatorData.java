package com.timnjonjo.democomic.data;

import com.timnjonjo.democomic.annotations.ValidEnum;
import com.timnjonjo.democomic.entities.Creator;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author TMwaura on 03/10/2020
 * @Project demo-comic
 */
@Data
@ToString
public class CreatorData {
    private Long id;
    @NotNull(message = "Salutation Cannot Be null")
    @ValidEnum(enumToValidate = Salutations.class, message = "Invalid Salutation")
    private String salutation;
    @NotNull(message = "Salutation Cannot Be null")
    private String firstName;
    @NotNull(message = "Salutation Cannot Be null")
    private String lastName;
    private boolean active;
    private Date createdAt;
    private Date updatedAt;

    public CreatorData(Long id, String salutation, String firstName, String lastName, boolean active, Date createdAt, Date updatedAt) {
        this.id = id;
        this.salutation = salutation;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Creator transform(){
        return new Creator(this.id, this.salutation, this.firstName, this.lastName, this.active);
    }
}
