package com.timnjonjo.democomic.data;

import com.timnjonjo.democomic.entities.Comic;
import com.timnjonjo.democomic.entities.Story;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author TMwaura on 04/10/2020
 * @Project demo-comic
 */
@Data
@ToString
@NoArgsConstructor
public class StoryData {
    private Long id;
    private String title;
    private String shortDescription;
    private String text;
    private Date createdAt;
    private Date updatedAt;
    private Integer comicId;

    public StoryData(Long id, String title, String shortDescription, String text, Date createdAt, Date updatedAt, Integer comicId) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.text = text;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.comicId = comicId;
    }


    public Story transform(){
        return new Story(id, title, shortDescription, text, new Comic(this.id));
    }
}
