package com.timnjonjo.democomic.api;

import com.timnjonjo.democomic.data.ResponseModel;
import com.timnjonjo.democomic.data.StoryData;
import com.timnjonjo.democomic.entities.Story;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.StoryNotFoundException;
import com.timnjonjo.democomic.services.StoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */

@RestController
@RequestMapping(value = "/v1/stories")
public class StoriesControllerApi {

    @Autowired
    StoriesService storiesService;

    @GetMapping
    public ResponseEntity findAll(){
        Collection<Story> stories = storiesService.findAll();
        if (stories == null || stories.isEmpty()) {
            return new ResponseModel(false, null, "No Stories Found", HttpStatus.NOT_FOUND).response();
        }
        return new ResponseModel(true, stories, String.format("Found %s Stories", stories.size()), HttpStatus.OK).response();
    }


    @PostMapping
    public ResponseEntity create(@RequestBody @Valid StoryData  storyData) {
        try {
            Story story = storiesService.create(storyData);
            return new ResponseModel(true, story, String.format("Story %s Created", story.getTitle()), HttpStatus.OK).response();
        } catch (CreateCharacterException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.BAD_REQUEST).response();
        }
    }

    @GetMapping(value = "getById")
    public ResponseEntity findById(@RequestParam Integer storyId){
        try {
            Story story = storiesService.getById(storyId);
            return new ResponseModel(true, story, String.format("Story with Id: %s Found", storyId), HttpStatus.OK).response();
        } catch (StoryNotFoundException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }

    @GetMapping(value = "getByComicId")
    public ResponseEntity getByComicId(@RequestParam Integer comicId){
        try {
            Collection<Story>  stories = storiesService.getByComicId(comicId);
            if(stories.isEmpty()){
                return new ResponseModel(false, null, "No Stories Found", HttpStatus.NOT_FOUND).response();
            }
            return new ResponseModel(true, stories, String.format("Stories with comicId: %s Found", comicId), HttpStatus.OK).response();
        } catch (StoryNotFoundException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }
}
