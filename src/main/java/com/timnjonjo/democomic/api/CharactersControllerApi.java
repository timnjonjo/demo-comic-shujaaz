package com.timnjonjo.democomic.api;

import com.timnjonjo.democomic.data.CharacterData;
import com.timnjonjo.democomic.data.ResponseModel;
import com.timnjonjo.democomic.entities.Character;
import com.timnjonjo.democomic.exceptions.CreateCharacterException;
import com.timnjonjo.democomic.exceptions.CharacterNotFoundExeption;
import com.timnjonjo.democomic.services.CharactersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */

@RestController
@RequestMapping(value = "/v1/chatacters")
public class CharactersControllerApi {

    @Autowired
    CharactersService charactersService;

    @GetMapping
    public ResponseEntity findAll(){
        Collection<Character> characters = charactersService.findAll();
        if (characters == null || characters.isEmpty()) {
            return new ResponseModel(false, null, "No Characters Found", HttpStatus.NOT_FOUND).response();
        }
        return new ResponseModel(true, characters, String.format("Found %s Characters", characters.size()), HttpStatus.OK).response();
    }


    @PostMapping
    public ResponseEntity create(@RequestBody @Valid CharacterData characterData) {
        try {
            Character character = charactersService.create(characterData);
            return new ResponseModel(true, character, String.format("Character %s Created", character.getCharacterName()), HttpStatus.OK).response();
        } catch (CreateCharacterException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.BAD_REQUEST).response();
        }
    }

    @GetMapping(value = "getById")
    public ResponseEntity findById(@RequestParam Integer storyId){
        try {
            Character character = charactersService.getById(storyId);
            return new ResponseModel(true, character, String.format("Character with Id: %s Found", storyId), HttpStatus.OK).response();
        } catch (CharacterNotFoundExeption e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }

    @GetMapping(value = "getByComicId")
    public ResponseEntity getByComicId(@RequestParam Integer comicId){
        try {
            Collection<Character> characters = charactersService.getByComicId(comicId);
            if(characters.isEmpty()){
                return new ResponseModel(false, null, "No Characters Found", HttpStatus.NOT_FOUND).response();
            }
            return new ResponseModel(true, characters, String.format("Characters with  ComicId: %s Found", comicId), HttpStatus.OK).response();
        } catch (CharacterNotFoundExeption e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }
}
