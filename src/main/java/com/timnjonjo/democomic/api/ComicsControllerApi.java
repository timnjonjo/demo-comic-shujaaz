package com.timnjonjo.democomic.api;

import com.timnjonjo.democomic.data.ResponseModel;
import com.timnjonjo.democomic.entities.Comic;
import com.timnjonjo.democomic.exceptions.ComicCreationException;
import com.timnjonjo.democomic.exceptions.ComicNotFoundException;
import com.timnjonjo.democomic.services.ComicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@RestController
@RequestMapping(value = "/v1/comics")
public class ComicsControllerApi {

    @Autowired
    ComicsService comicsService;

    @GetMapping
    public ResponseEntity findAll(){
        Collection<Comic> comics = comicsService.findAll();
        if (comics == null || comics.isEmpty()) {
            return new ResponseModel(false, null, "No Comics Found", HttpStatus.NOT_FOUND).response();
        }
        return new ResponseModel(true, comics, String.format("Found %s Comics", comics.size()), HttpStatus.OK).response();
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid Comic comicData) {
        try {
            Comic comic = comicsService.create(comicData);
            return new ResponseModel(true, comic, String.format("Comic %s Created", comic.getTitle()), HttpStatus.OK).response();
        } catch (ComicCreationException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.BAD_REQUEST).response();
        }
    }

    @GetMapping(value = "getById")
    public ResponseEntity findById(@RequestParam Integer comicId){
        try {
            Comic comic = comicsService.getById(comicId);
            return new ResponseModel(true, comic, String.format("Creator with Id: %s Found", comicId), HttpStatus.OK).response();
        } catch (ComicNotFoundException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }



}
