package com.timnjonjo.democomic.api;

import com.timnjonjo.democomic.data.CreatorData;
import com.timnjonjo.democomic.data.ResponseModel;
import com.timnjonjo.democomic.entities.Creator;
import com.timnjonjo.democomic.exceptions.CreatorCreationException;
import com.timnjonjo.democomic.exceptions.CreatorNotFundException;
import com.timnjonjo.democomic.services.CreatorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author TMwaura on 05/10/2020
 * @Project demo-comic
 */
@RestController
@RequestMapping(value = "/v1/creators")
public class CreatorsControllerApi {

    @Autowired
    CreatorsService creatorsService;

    @GetMapping
    public ResponseEntity findAll() {
        Collection<Creator> creators = creatorsService.findAll();
        if (creators == null || creators.isEmpty()) {
            return new ResponseModel(false, null, "No Creators Found", HttpStatus.NOT_FOUND).response();
        }
        return new ResponseModel(true, creators.stream().map(Creator::transform).collect(Collectors.toList()), String.format("Found %s Creators", creators.size()), HttpStatus.OK).response();
    }

    @GetMapping(value = "getByCreatorId")
    public ResponseEntity findById(@RequestParam Integer creatorId){
        try {
            Creator creator = creatorsService.getById(creatorId);
            return new ResponseModel(true, creator.transform(), String.format("Creator with Id: %s Found", creatorId), HttpStatus.OK).response();
        } catch (CreatorNotFundException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.NOT_FOUND).response();
        }
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid CreatorData creatorData) {
        try {
            Creator creator = creatorsService.create(creatorData);
            return new ResponseModel(true, creator.transform(), String.format("Creator %s Created", creator.getFullName()), HttpStatus.OK).response();
        } catch (CreatorCreationException e) {
            return new ResponseModel(false, null, e.getMessage(), HttpStatus.BAD_REQUEST).response();
        }
    }


}
